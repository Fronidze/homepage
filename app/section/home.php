<!-- Home Subpage -->
<section data-id="home" class="animated-section start-page">
    <div class="section-content vcentered">

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="title-block">
                    <h2><?= person()->fullName() ?></h2>
                    <div class="owl-carousel text-rotation">
                        <div class="item"><div class="sp-subtitle"><?= person()->position() ?></div></div>
                        <?php foreach (person()->listPositions() as $position): ?>
                            <div class="item"><div class="sp-subtitle"><?= $position ?></div></div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- End of Home Subpage -->