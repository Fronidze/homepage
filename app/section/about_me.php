<!-- About Me Subpage -->
<section data-id="about-me" class="animated-section">
    <div class="section-content">
        <div class="page-title">
            <h2><?= translate('title.about_me')?></h2>
        </div>

        <!-- Personal Information -->
        <div class="row">
            <div class="col-xs-12 col-sm-7" style="padding-left: 0">
                <?= translate('text.about_me_short')?>
            </div>
            <div class="col-xs-12 col-sm-5">
                <div class="info-list">
                    <ul>
                        <li>
                            <span class="title"><?= translate('age')?></span>
                            <span class="value"><?= person()->age() ?></span>
                        </li>

                        <li>
                            <span class="title"><?= translate('residence')?></span>
                            <span class="value"><?= person()->residence() ?></span>
                        </li>

                        <li>
                            <span class="title"><?= translate('address')?></span>
                            <span class="value"><?= person()->address() ?></span>
                        </li>

                        <li>
                            <span class="title"><?= translate('email')?></span>
                            <span class="value"><a href="mailto:<?= person()->email()?>"><?= person()->email()?></a></span>
                        </li>

                        <li>
                            <span class="title"><?= translate('phone')?></span>
                            <span class="value"><a href="tel:<?= person()->phone()?>"><?= person()->phone(true)?></a></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <?= translate('text.about_me_full')?>
            </div>
        </div>
        <!-- End of Personal Information -->

        <div class="white-space-50"></div>

        <!-- Services -->
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <div class="block-title">
                    <h3>What <span>I Do</span></h3>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="col-inner">
                    <div class="info-list-w-icon">
                        <div class="info-block-w-icon">
                            <div class="ci-icon">
                                <i class="lnr lnr-store"></i>
                            </div>
                            <div class="ci-text">
                                <h4>Ecommerce</h4>
                                <p>Pellentesque pellentesque, ipsum sit amet auctor accumsan, odio tortor bibendum massa, sit amet ultricies ex lectus scelerisque nibh. Ut non sodales.</p>
                            </div>
                        </div>
                        <div class="info-block-w-icon">
                            <div class="ci-icon">
                                <i class="lnr lnr-laptop-phone"></i>
                            </div><div class="ci-text">
                                <h4>Web Design</h4>
                                <p>Pellentesque pellentesque, ipsum sit amet auctor accumsan, odio tortor bibendum massa, sit amet ultricies ex lectus scelerisque nibh. Ut non sodales.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6">
                <div class="col-inner">
                    <div class="info-list-w-icon">
                        <div class="info-block-w-icon">
                            <div class="ci-icon">
                                <i class="lnr lnr-pencil"></i>
                            </div>
                            <div class="ci-text">
                                <h4>Copywriting</h4>
                                <p>Pellentesque pellentesque, ipsum sit amet auctor accumsan, odio tortor bibendum massa, sit amet ultricies ex lectus scelerisque nibh. Ut non sodales.</p>
                            </div>
                        </div>
                        <div class="info-block-w-icon">
                            <div class="ci-icon">
                                <i class="lnr lnr-flag"></i>
                            </div><div class="ci-text">
                                <h4>Management</h4>
                                <p>Pellentesque pellentesque, ipsum sit amet auctor accumsan, odio tortor bibendum massa, sit amet ultricies ex lectus scelerisque nibh. Ut non sodales.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Services -->

        <div class="white-space-30"></div>

        <!-- Clients -->
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <div class="block-title">
                    <h3>Cilents</h3>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <div class="clients owl-carousel">

                    <div class="client-block">
                        <a href="#" target="_blank" title="Logo">
                            <img src="img/clients/client-1.png" alt="Logo">
                        </a>
                    </div>

                    <div class="client-block">
                        <a href="#" target="_blank" title="Logo">
                            <img src="img/clients/client-2.png" alt="Logo">
                        </a>
                    </div>

                    <div class="client-block">
                        <a href="#" target="_blank" title="Logo">
                            <img src="img/clients/client-3.png" alt="Logo">
                        </a>
                    </div>

                    <div class="client-block">
                        <a href="#" target="_blank" title="Logo">
                            <img src="img/clients/client-4.png" alt="Logo">
                        </a>
                    </div>

                    <div class="client-block">
                        <a href="#" target="_blank" title="Logo">
                            <img src="img/clients/client-5.png" alt="Logo">
                        </a>
                    </div>

                    <div class="client-block">
                        <a href="#" target="_blank" title="Logo">
                            <img src="img/clients/client-6.png" alt="Logo">
                        </a>
                    </div>

                    <div class="client-block">
                        <a href="#" target="_blank" title="Logo">
                            <img src="img/clients/client-7.png" alt="Logo">
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <!-- End of Clients -->

        <div class="white-space-50"></div>

        <!-- Fun Facts -->
        <div class="row">
            <div class="col-xs-12 col-sm-12">

                <div class="block-title">
                    <h3>Fun <span>Facts</span></h3>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="fun-fact gray-default">
                    <i class="lnr lnr-heart"></i>
                    <h4>Happy Clients</h4>
                    <span class="fun-fact-block-value">578</span>
                    <span class="fun-fact-block-text"></span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4">
                <div class="fun-fact gray-default">
                    <i class="lnr lnr-clock"></i>
                    <h4>Working Hours</h4>
                    <span class="fun-fact-block-value">4,780</span>
                    <span class="fun-fact-block-text"></span>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 ">
                <div class="fun-fact gray-default">
                    <i class="lnr lnr-star"></i>
                    <h4>Awards Won</h4>
                    <span class="fun-fact-block-value">15</span>
                    <span class="fun-fact-block-text"></span>
                </div>
            </div>
        </div>
        <!-- End of Fun Facts -->

    </div>
</section>
<!-- End of About Me Subpage -->