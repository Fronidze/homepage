<?php

use app\entity\Person;
use app\translate\Translate;
use Dotenv\Dotenv;
use Illuminate\Container\Container;

$envFile = dirname(__DIR__) . '/.env';
if (file_exists($envFile)) {
    Dotenv::createUnsafeImmutable(dirname(__DIR__))
        ->load();
}

$container = app();

$container->instance('language', env('LANG'));

$container->singleton(Translate::class, function (Container $container) {
    $translate = new Translate($container->make('language'));
    $translate->loadMessages();
    return $translate;
});

$container->bind(Person::class, function (Container $container) {
    return new Person(
        \translate('last_name'),
        \translate('first_name')
    );
});