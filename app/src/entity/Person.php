<?php

namespace app\entity;

use JetBrains\PhpStorm\Pure;

class Person
{
    public function __construct(private string $lastName, private string $firstName,)
    {
    }

    public function fullName(): string
    {
        return "$this->lastName $this->firstName";
    }

    public function age(): int
    {
        $datetime = new \DateTime();
        return $datetime->diff(new \DateTime('1988-09-15'))->y;
    }

    public function residence(): string
    {
        return translate('person.residence');
    }

    #[Pure] public function position(): string
    {
        return translate('backend_developer');
    }

    public function listPositions(): array
    {
        return [
            translate('frontend_developer'),
            translate('devops'),
            translate('nice_guy'),
        ];
    }

    public function socialLinks(): array
    {
        return [
            ['link' => 'https://vk.com/id2427515', 'icon' => 'fab fa-vk'],
            ['link' => 'https://gitlab.com/Fronidze', 'icon' => 'fab fa-gitlab'],
            ['link' => 'https://hub.docker.com/u/fronidze', 'icon' => 'fab fa-docker'],
            ['link' => 'https://www.linkedin.com/in/%D0%B0%D0%BB%D0%B5%D0%BA%D1%81%D0%B5%D0%B9-%D0%BC%D0%B0%D1%80%D0%BA%D0%B5%D0%BB%D0%BE%D0%B2-229157106/', 'icon' => 'fab fa-linkedin-in'],
        ];
    }

    public function downloadCvLink(): string
    {
        return '/files/markelov.pdf';
    }

    public function address(): string
    {
        return translate('person.address');
    }

    public function email(): string
    {
        return 'info@fronidze.com';
    }

    public function phone(bool $isPretty = false): string
    {
        if ($isPretty === true) {
            return '+7 (906) 143-9036';
        }
        return '+79061439036';
    }
}