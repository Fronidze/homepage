<?php

namespace app\translate;

use JetBrains\PhpStorm\NoReturn;

class Translate
{
    private array $messages = [];
    public function __construct(
        protected string $language
    )
    {

    }

    #[NoReturn] public function loadMessages(string|null $path = null): void
    {
        $filePath = __DIR__."/lang/{$this->language}/message.php";
        if (file_exists($filePath) === false) {
            throw new \LogicException('Файл: '.$filePath.' с переводами не найден');
        }

        $this->messages = require $filePath;
    }

    public function translate(string $key): string
    {
        if (array_key_exists($key, $this->messages) === false) {
            return $key;
        }

        return $this->messages[$key];
    }
}