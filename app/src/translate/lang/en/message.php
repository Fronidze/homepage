<?php

$aboutMeText = <<<ABOUT
<p>
    My name is Aleksey Markelov, at the moment I am a backend developer in PHP and I am actively studying
    golang. My first acquaintance with programming took place in 2013, when at the University I had to pass
    laboratory. There I first got acquainted with the concepts of OOP, as well as with C#. Actively began to study new technologies and what is Web and Mobile development.
    Interest fell on Web development namely, on HTML / CSS / JS, after which I realized that it’s not interesting without dynamic content. 
</p>
ABOUT;

$aboutMeTextFull = <<<ABOUT
<p>
I switched to learning PHP, at first I wrote in pure language using OOP principles, then I switched to using frameworks Yii2 / Laravel.
   When developing Backend for Web applications, you face the problem of how to deliver all this to the production server.
   Colleagues advised to look at the Docker side, at first it was not clear what it was and what it was eaten with, but quickly
   I realized what a convenient tool it is and now almost 98% of my development begins with the fact that the environment is going to
   Docker containers and after that I just deploy projects. Of course Docker is good, but rollout automation has become mine
   next step. I actively use Gitlab, so I studied Gitlab CI / CD for my projects, and I also try to recommend
   to their clients to use CI/CD in their projects.
</p>
<p>
    Often there are moments that you need to edit Frontend applications, but there were not always developers at hand. That's why
     several projects had to be both Backend and Frontend developers. I use Frontend Framework for my developments.
     Angular. Often, the modern Web requires real-time response without reloading the page. For these purposes, I used
     Node JS to work with both TCP sockets and Web sockets. I have experience with setting up Webpack, but it is insignificant, so to speak.
     I can set it up with documentation.
</p> 
<p>
</p>
    Recently, I realized that Node JS can be replaced with Golang, I really like the conciseness and simplicity of this language. What he
     compiled and quickly integrated into the service architecture. There are plans to move from Docker / Docker Swarm to Kubernetes.
ABOUT;

return [
    'last_name' => 'Markelov',
    'first_name' => 'Aleksey',
    'middle_name' => 'Yurievich',
    'age' => 'Age',
    'backend_developer' => 'Backend Developer',
    'frontend_developer' => 'Frontend Developer',
    'devops' => 'Little DevOps',
    'nice_guy' => 'and just nice guy ... ',
    'download_cv' => 'Download CV',
    'all_rights_reserved' => 'All rights reserved',
    'menu.home' => 'Home',
    'menu.about_me' => 'About Me',
    'menu.resume' => 'Resume',
    'menu.contact' => 'Contact',
    'title.about_me' => 'About <span>me</span>',
    'text.about_me_short' => $aboutMeText,
    'text.about_me_full' => $aboutMeTextFull,
];