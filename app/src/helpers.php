<?php

use app\entity\Person;
use app\translate\Translate;
use Illuminate\Container\Container;
use JetBrains\PhpStorm\Pure;


if (function_exists('person') === false) {
    #[Pure]
    function person(): Person {
        return app()->make(Person::class);
    }
}

if (function_exists('env') === false) {
    function env(string $key, $default = null) {
        $value = getenv($key);
        return $value !== false ? $value : $default;
    }
}

if (function_exists('translate') === false) {
    function translate(string $key): string {
        /** @var Translate $translate */
        $translate = app()->make(Translate::class);
        return $translate->translate($key);
    }
}

if (function_exists('app') === false) {
    function app(): Container {
        return Container::getInstance();
    }
}